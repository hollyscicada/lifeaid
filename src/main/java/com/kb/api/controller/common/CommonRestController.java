package com.kb.api.controller.common;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class CommonRestController {   
	
	@Inject MasterService masterService; 
	 
	   
	/**
	 * 공통코드
	 */
	@PostMapping("/api/v1/common")   
	public ResponseEntity<Map<String, Object>> common(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 

		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.CommonMapper", "list", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**
	 * 메인 대문이미지 리스트
	 */
	@PostMapping("/api/v1/main/img")   
	public ResponseEntity<Map<String, Object>> main_img(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.CommonMapper", "main_img_list", paramMap);
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
