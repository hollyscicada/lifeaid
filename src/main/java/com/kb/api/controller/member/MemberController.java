package com.kb.api.controller.member;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.kb.api.service.MasterService;

@Controller
public class MemberController {  
	
	@Inject MasterService masterService;
	
	@GetMapping("/findId") 
	public String findId(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "notLayout/member/findId";
	}
	
	@GetMapping("/findPw") 
	public String findPw(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "notLayout/member/findPw";
	}
	
	@GetMapping("/join") 
	public String join(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "notLayout/member/join";
	}
	@PostMapping("/join2") 
	public String join2(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "notLayout/member/join2";
	}
	@GetMapping("/join3") 
	public String join3(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		return "notLayout/member/join3";
	}
}
