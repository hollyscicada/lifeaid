package com.kb.api.controller.cs;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class CsRestController {   
	  
	@Inject MasterService masterService; 
	 
	   
	/**
	 * cs 리스트 
	 */
	@PostMapping("/api/v1/cs")   
	public ResponseEntity<Map<String, Object>> cs(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block()); 
		param.put("page_block", param.getPage_block()); 

		HashMap paramMap = new HashMap(param);
		
		
		int record = masterService.dataCount("mapper.CsMapper", "list_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.CsMapper", "list", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * cs 추가 
	 */
	@PostMapping("/api/v1/cs/create")   
	public ResponseEntity<Map<String, Object>> cs_create(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		int record = masterService.dataCreate("mapper.CsMapper", "create", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * cs 수정
	 */
	@PostMapping("/api/v1/cs/update")   
	public ResponseEntity<Map<String, Object>> cs_update(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		int record = masterService.dataUpdate("mapper.CsMapper", "update", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * cs 삭제 
	 */
	@PostMapping("/api/v1/cs/delete")   
	public ResponseEntity<Map<String, Object>> cs_delete(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);

		HashMap paramMap = new HashMap(param);
		
		int record = masterService.dataUpdate("mapper.CsMapper", "delete", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
