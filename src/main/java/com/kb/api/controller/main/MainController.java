package com.kb.api.controller.main;

import java.net.URLEncoder;
import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.kb.api.service.MasterService;
import com.kb.api.service.main.MainService; 

@Controller
public class MainController {  
	
	@Inject MainService mainService; 
	@Inject MasterService masterService;  
	@GetMapping("") 
	public String main(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session) throws Exception {
		mainService.main(model);
		return "layout/main/main";
	}
	
	@GetMapping("/sub/{category_seq}") 
	public String sub(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @PathVariable String category_seq) throws Exception {
		model.addAttribute("category_seq", category_seq);
		mainService.sub(model, category_seq, session);
		return "layout/main/sub";
	}
	
	@GetMapping("/sub/detail/{content_seq}") 
	public String subDetail(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session, @PathVariable String content_seq) throws Exception {
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		if(sessionMap == null) {
			
			String redirect_url = URLEncoder.encode(request.getRequestURI(), "UTF-8");

			return "redirect:/login?redirect_url="+redirect_url;
		}
		
		model.addAttribute("content_seq", content_seq);
		mainService.subDetail(model, content_seq, session);
		return "layout/main/subDetail";
	}
}
