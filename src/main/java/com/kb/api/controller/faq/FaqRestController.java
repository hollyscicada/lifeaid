package com.kb.api.controller.faq;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class FaqRestController {   
	  
	@Inject MasterService masterService; 
	 
	   
	/**
	 * faq 리스트 
	 */
	@PostMapping("/api/v1/faq")   
	public ResponseEntity<Map<String, Object>> cs(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		 
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block()); 
		param.put("page_block", param.getPage_block()); 

		HashMap paramMap = new HashMap(param);
		
		
		int record = masterService.dataCount("mapper.FaqMapper", "list_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.FaqMapper", "list", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
