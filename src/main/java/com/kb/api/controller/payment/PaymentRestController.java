package com.kb.api.controller.payment;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class PaymentRestController {   
	
	@Inject MasterService masterService; 
	 
	   
	/** 
	 * 프리미엄 구독 상품 리스트
	 */
	@PostMapping("/api/v1/premium")  
	public ResponseEntity<Map<String, Object>> preminu(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		  
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.PaymentMapper", "premium", paramMap);
     		
		 
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
}
