package com.kb.api.controller.category;

import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {
	
	/*
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> globalExceptionHandler(Exception ex, WebRequest request) {
		ResponseEntity<Object> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		map.put("erroMsg", "시스템 오류가 발생하였습니다. 잠시후 다시 시도해주세요.");

		entity = new ResponseEntity<Object>(map, HttpStatus.BAD_REQUEST);
		return entity;
	}
	*/
}
