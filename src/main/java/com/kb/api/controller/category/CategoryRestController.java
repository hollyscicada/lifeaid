package com.kb.api.controller.category;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class CategoryRestController {   
	
	@Inject MasterService masterService; 
	 
	   
	/** 
	 * 카테고리 리스트
	 */
	@PostMapping("/api/v1/category")  
	public ResponseEntity<Map<String, Object>> category(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.CategoryMapper", "list", paramMap);
     		
		map.put("data", retMap);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		
		return entity; 
	}
	
	/**
	 * 카테고리2 리스트
	 */
	@PostMapping("/api/v1/category/info")  
	public ResponseEntity<Map<String, Object>> category_info(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.CategoryMapper", "category_info", paramMap);
     		
		 
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK); 
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 카테고리3 구분 리스트
	 */
	@PostMapping("/api/v1/category/gubun")  
	public ResponseEntity<Map<String, Object>> category_gubun(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		
		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.CategoryMapper", "category_gubun", paramMap);
		
		
		map.put("data", retMap);
		
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
