package com.kb.api.controller.content;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class ContentRestController {   
	
	@Inject MasterService masterService; 
	 
	/** 
	 * 컨텐츠 리스트
	 */
	@PostMapping("/api/v1/content")     
	public ResponseEntity<Map<String, Object>> content(HttpSession session, @RequestBody ProHashMap param) throws Exception { 
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		if(sessionMap != null) { 
			param.put("member_seq", sessionMap.get("member_seq"));
		}
		
		
		ResponseEntity<Map<String, Object>> entity = null; 
		Map<String, Object> map = new HashMap<String, Object>();    
		  
		param.setPage(param.get("page") != null ? Integer.parseInt(param.get("page").toString()) : param.getPage());
		param.setPage_block(param.get("page_block") != null ? Integer.parseInt(param.get("page_block").toString()) : param.getPage_block());
		param.put("pg", (Integer.parseInt(param.get("page").toString())-1)*param.getPage_block());  
		param.put("page_block", param.getPage_block()); 
 
		HashMap paramMap = new HashMap(param);
		
		
		int record = masterService.dataCount("mapper.ContentMapper", "content_cnt", paramMap);
		List<HashMap<String, Object>> retMap = (List) masterService.dataList("mapper.ContentMapper", "content", paramMap);
		param.setTotalCount(record); // 게시물 총 개수
		
		map.put("paging", param.getPageIngObj());
		map.put("data", retMap);
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	  
	    
	
	/**
	 * 연관 컨텐츠 리스트
	 */ 
	@PostMapping("/api/v1/content/relation")      
	public ResponseEntity<Map<String, Object>> content_relation(HttpSession session, @RequestBody ProHashMap param) throws Exception { 
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		if(sessionMap != null) { 
			param.put("member_seq", sessionMap.get("member_seq"));
		}
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>(); 
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null; 
		retMap = (List) masterService.dataList("mapper.ContentMapper", "relation_content", paramMap);
		map.put("data", retMap);
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 최근 열람한 컨텐츠 리스트
	 */
	@PostMapping("/api/v1/content/view")   
	public ResponseEntity<Map<String, Object>> content_view(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.ContentMapper", "view_content", paramMap);
		map.put("data", retMap);
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 열람한 컨텐츠 삭제
	 */
	@PostMapping("/api/v1/content/view/delete")   
	public ResponseEntity<Map<String, Object>> content_view_delete(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		int record = masterService.dataCreate("mapper.ContentMapper", "view_content_delete", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 좋아요 컨텐츠 리스트
	 */
	@PostMapping("/api/v1/content/goods")   
	public ResponseEntity<Map<String, Object>> content_goods(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.ContentMapper", "goods_content", paramMap);
		map.put("data", retMap);
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 좋아요 컨텐츠 삭제
	 */
	@PostMapping("/api/v1/content/goods/delete")   
	public ResponseEntity<Map<String, Object>> content_goods_delete(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		int record = masterService.dataCreate("mapper.ContentMapper", "goods_content_delete", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 루틴 컨텐츠 리스트
	 */ 
	@PostMapping("/api/v1/content/routine")   
	public ResponseEntity<Map<String, Object>> content_routine(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.ContentMapper", "routine_content", paramMap);
		map.put("data", retMap);
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 루틴 컨텐츠 삭제
	 */
	@PostMapping("/api/v1/content/routine/delete")   
	public ResponseEntity<Map<String, Object>> content_routine_delete(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		
		HashMap paramMap = new HashMap(param);
		int record = masterService.dataCreate("mapper.ContentMapper", "routine_content_delete", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
