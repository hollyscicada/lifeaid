package com.kb.api.controller.favorite;
 
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.kb.api.service.MasterService;
import com.kb.api.util.Common;
import com.kb.api.util.ProHashMap;

@RestController 
public class FavoritesRestController {   
	
	@Inject MasterService masterService; 
	 
	   
	
	/**
	 * 즐겨찾기 내가 생성한 폴더 리스트 / 폴더에 속한 즐겨찾기 컨텐츠
	 */
	@PostMapping("/api/v1/folder")   
	public ResponseEntity<Map<String, Object>> folder(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.FavoritesMapper", "folder_list", paramMap);
		
		for(int i = 0 ; i < retMap.size() ; i++) {
			retMap.get(i).put("favorites_list", (List) masterService.dataList("mapper.FavoritesMapper", "content_list", retMap.get(i)));
		}
     		
		 
		map.put("data", retMap);
		 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	
	/**
	 * 폴더에 속한 컨텐츠 불러오기
	 */
	@PostMapping("/api/v1/favorites/content")   
	public ResponseEntity<Map<String, Object>> folder_read(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();

		HashMap paramMap = new HashMap(param);
		
		List<HashMap<String, Object>> retMap = null;
		retMap = (List) masterService.dataList("mapper.FavoritesMapper", "content_list", paramMap);
		
		 
		map.put("data", retMap);
		 
		
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	/**
	 * 폴더추가
	 */
	@PostMapping("/api/v1/folder/create")    
	public ResponseEntity<Map<String, Object>> folder_create(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		int record = masterService.dataCreate("mapper.FavoritesMapper", "folder_create", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**
	 * 폴더 삭제
	 */
	@PostMapping("/api/v1/folder/delete")    
	public ResponseEntity<Map<String, Object>> folder_delete(HttpSession session, @RequestBody ProHashMap param) throws Exception {
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		int record = masterService.dataCreate("mapper.FavoritesMapper", "folder_delete", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity;  
	}
	
	/**
	 * 즐겨찾기 추가 
	 */
	@PostMapping("/api/v1/favorites/create")    
	public ResponseEntity<Map<String, Object>> favorites_create(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		 
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false); 
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		int record = masterService.dataCreate("mapper.FavoritesMapper", "favorites_create", paramMap);
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
	/**
	 * 즐겨찾기 삭제
	 */
	@PostMapping("/api/v1/favorites/delete")    
	public ResponseEntity<Map<String, Object>> favorites_delete(HttpSession session, @RequestBody ProHashMap param, HttpServletRequest request) throws Exception {
		String member_seq = new Common().getTokenInfo(request, "member_seq");
		param.put("member_seq", member_seq);
		
		
		ResponseEntity<Map<String, Object>> entity = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", false);
		HashMap paramMap = new HashMap(param);
		List<HashMap<String, Object>> retMap = null;
		int record = masterService.dataCreate("mapper.FavoritesMapper", "favorites_delete", paramMap);
		
		if(record > 0) {
			map.put("success", true);
		}
		entity = new ResponseEntity<Map<String, Object>>(map, HttpStatus.OK);
		Common.returnPrint(Common.GmakeDynamicValueObject(entity));
		return entity; 
	}
}
