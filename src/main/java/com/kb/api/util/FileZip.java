package com.kb.api.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;

public class FileZip {

	public void makeZip(String zipFilePath, String zipFileName) throws IllegalArgumentException, IOException {

		zipFileName += ".zip";
		
		
		/**
		 * zip  파일 만들기
		 */
		File d = new File(zipFilePath);
		String[] entries = d.list();

		byte[] buffer = new byte[4096];
		int bytesRead;

		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
		for (int i = 0; i < entries.length; i++) {
			
			String fileName = entries[i].toString();
			String extension = FilenameUtils.getExtension(fileName);
			
			/**
			 * 해당경로에서 xls만 zip파일로 만들기
			 */
			if(extension != null && extension.equals("xls")) {
			
				File f = new File(d, entries[i]);
				FileInputStream in = new FileInputStream(f);
				
				
				ZipEntry entry = new ZipEntry(fileName);
				out.putNextEntry(entry);
				while ((bytesRead = in.read(buffer)) != -1)
					out.write(buffer, 0, bytesRead);
				in.close();
			}
		}
		out.close();
		
		
		/**
		 * 생성된 xls 삭제
		 */
		for (int i = 0; i < entries.length; i++) {
			String fileName = entries[i].toString();
			String extension = FilenameUtils.getExtension(fileName);
			
			if(extension != null && extension.equals("xls")) {
				File s = new File(zipFilePath+fileName);
				s.delete();
			}
		}
		
	}
}
