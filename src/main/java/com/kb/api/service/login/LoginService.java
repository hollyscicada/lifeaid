package com.kb.api.service.login;

import java.util.HashMap;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.kb.api.persistence.MasterDao;
import com.kb.api.util.Common;


@Service
public class LoginService {
	@Inject MasterDao MasterDao;
	public String snsSign(String id, String login_type_code, HttpSession session){
		HashMap paramMap = new HashMap();
		paramMap.put("id", id);
		paramMap.put("login_type_code", login_type_code);
		
		HashMap<String, Object> retMap = null;
		retMap = (HashMap) MasterDao.dataRead("mapper.LoginMapper", "login_sns", paramMap);
		
		
		
		if(retMap == null) {
			int record = MasterDao.dataCreate("mapper.LoginMapper", "sign_sns", paramMap);
			if(record>0) {
				
				record = MasterDao.dataCreate("mapper.LoginMapper", "folder_default", paramMap); 
				record += MasterDao.dataCreate("mapper.LoginMapper", "folder_share", paramMap); 
				if(record == 2) {
					memberSesstion(retMap, session, paramMap);
					return "redirect:/join3";
				}else {
					return null;
				}
			}else {
				return null;
			}
		}else {
			memberSesstion(retMap, session, paramMap);
			//로그인시
			return "redirect:/";
		}
	}
	
	public void memberSesstion(HashMap<String, Object> retMap, HttpSession session, HashMap paramMap) {
		retMap = (HashMap) MasterDao.dataRead("mapper.LoginMapper", "user_info_id", paramMap);
		retMap.put("authToken", new Common().makeToken(retMap.get("member_seq").toString()));
		retMap.put("custom_pw", retMap.get("custom_pw"));
		session.setAttribute("MEMBER", retMap);
	}
}
