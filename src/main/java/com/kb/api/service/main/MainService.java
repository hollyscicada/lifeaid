package com.kb.api.service.main;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import com.kb.api.persistence.MasterDao;
@Service
public class MainService {
	@Inject MasterDao MasterDao;
	
	
	public void main(Model model){ 
		banner(model);
		menu(model);
	}
	public void sub(Model model, String category_seq, HttpSession session){ 
		
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		
		
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("category_seq", category_seq); 
		 
		if(sessionMap != null) { 
			rstMap.put("member_seq", sessionMap.get("member_seq"));
		}
		 
		menu(model);  
		rightBar(model);
		/**
		 * 카테고리 정보 가져오기 
		 */ 
		HashMap<String, Object> categoryRead = (HashMap<String, Object>) MasterDao.dataRead("mapper.CategoryMapper", "read", rstMap);
		model.addAttribute("categoryRead", categoryRead);
		
		/**
		 * 오늘의 스팟 컨텐츠 가져오기
		 */
		HashMap<String, Object> todayContent = (HashMap<String, Object>) MasterDao.dataRead("mapper.ContentMapper", "today_content", rstMap);
		model.addAttribute("todayContent", todayContent);
		
		
		/**
		 * adzone 가져오기
		 */
		HashMap<String, Object> adzoneRead = (HashMap<String, Object>) MasterDao.dataRead("mapper.AdzoneMapper", "read", rstMap);
		model.addAttribute("adzoneRead", adzoneRead);
		
		/**
		 * 해시태그 가져오기
		 */
		List<HashMap<String, Object>> hashTagList = (List) MasterDao.dataList("mapper.HashtagMapper", "list", rstMap);
		model.addAttribute("hashTagList", hashTagList);
		
	}
	
	@Transactional("transactionManagerMaster")
	public void subDetail(Model model, String content_seq, HttpSession session){ 
		
		menu(model);
		rightBar(model); 
		
		HashMap<String, Object> sessionMap = null;
		sessionMap = (HashMap<String, Object>) session.getAttribute("MEMBER");
		
		
		HashMap<String, Object> rstMap = new HashMap<String, Object>();
		rstMap.put("content_seq", content_seq); 
		 
		if(sessionMap != null) { 
			rstMap.put("member_seq", sessionMap.get("member_seq"));
		}
		
		/**
		 * 컨텐츠 조회수 증가
		 */
		MasterDao.dataCreate("mapper.ContentMapper", "content_read_view_create", rstMap); 
		
		/**
		 * 컨텐츠 상세 정보 가져오기
		 */
		HashMap<String, Object> contentRead =(HashMap<String, Object>) MasterDao.dataRead("mapper.ContentMapper", "content_read", rstMap);
		model.addAttribute("contentRead", contentRead);
	}
	
	
	
	public void banner(Model model){
		
		/**
		 * 메인 배너리스트 가져오기
		 */
		List<HashMap<String, Object>> bannerList = (List) MasterDao.dataList("mapper.CommonMapper", "main_img_list", null);
		model.addAttribute("bannerList", bannerList);
	}
	public void menu(Model model){ 
		
		/**
		 * 상단 메뉴를 만들기위한 카테고리와 카테고리 info 리스트 가져오기
		 */
		List<HashMap<String, Object>> categoryList = (List) MasterDao.dataList("mapper.CategoryMapper", "list", null);
		for(int i = 0 ; i < categoryList.size() ; i++) {
			HashMap paramMap = new HashMap<String, Object>();
			paramMap.put("category_seq", categoryList.get(i).get("category_seq"));
			List<HashMap<String, Object>> infoList = (List) MasterDao.dataList("mapper.CategoryMapper", "category_info", paramMap);
			categoryList.get(i).put("infoList", infoList);
		}
		model.addAttribute("categoryList", categoryList);
	}
	public void rightBar(Model model) {
		/**
		 * 인기있는 컨텐츠 가져오기
		 */
		List<HashMap<String, Object>> bestContentList = (List) MasterDao.dataList("mapper.ContentMapper", "best_content", null);
		model.addAttribute("bestContentList", bestContentList);
		 
		/**
		 * 최신 ebook 컨텐츠 가져오기
		 */
		List<HashMap<String, Object>> newEbookContent = (List) MasterDao.dataList("mapper.ContentMapper", "new_ebook_content", null);
		model.addAttribute("newEbookContent", newEbookContent);
	}
}
