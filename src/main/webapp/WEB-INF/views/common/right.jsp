<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<!-- 오른쪽 영상리스트 -->
   <div class="right_list">
        <h3>인기있는 영상</h3>
        <c:forEach var="item" items="${bestContentList }">
        	<div class="video content-obj" data-seq="${item.content_seq}">
<%--         	<div class="thumbnail" style="background-image: url('${item.thumb_url}');background-size: 100% 100%;"> --%>
	            <div class="thumbnail" >
	            	<img src="${item.thumb_url}" style="width: 100%;">
	            </div>
	            <div class="info">
	                <p>${item.title }</p>
	                <div>
	                    <img src="/resources/img/icon-play.svg" alt="재생횟수">
	                    <span>${item.view_cnt }</span>
	                </div>
	            </div>
	        </div>
        </c:forEach>
        
        <h3>최신 E-book</h3>
        <c:forEach var="item" items="${newEbookContent }">
	        <div class="video ebook-obj" data-seq="${item.content_seq}">
<%-- 	            <div class="thumbnail" style="background-image: url('${item.thumb_url}');background-size: 100% 100%;"></div> --%>
	            <div class="thumbnail" >
	            	<img src="${item.thumb_url}" style="width: 100%;">
	            </div>
	            <div class="info">
	                <p>${item.title }</p>
	                <div>
	                    <img src="/resources/img/icon-play.svg" alt="재생횟수">
	                    <span>${item.view_cnt }</span>
	                </div>
	            </div>
	        </div>
        </c:forEach>
   </div>
