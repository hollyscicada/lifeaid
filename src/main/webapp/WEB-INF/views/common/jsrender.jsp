<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="https://www.jsviews.com/download/jsrender.js"></script>
<script>
$.views.converters("dec",
        function(val) {
          return global.comma(val);
        }
      );
$.views.converters("eval",
        function(val) {
          return val;
        }
      );
      
$.views.helpers("hashtag",
        function(val) {
			if(val){
				var hashtags = val.split("|");
				var hashTagList = [];
				for(var i = 0 ; i < hashtags.length ; i++){
					var param = {
							"name":hashtags[i]
					}
					hashTagList.push(param);
					
				}
				return hashTagList;
			}
			return null;
        }
      );
</script>
<script>
	$(document).on("click", ".content-obj", function(){
		var seq = $(this).attr("data-seq");
		location.href="/sub/detail/"+seq;
	})
</script>
<style>
	.content-obj{
		cursor: pointer;
	}
</style>
<script type="text/x-jsrender" id="accessCode">
		{{for data }}
			<div class="video_item content-obj" data-seq="{{:content_seq}}">
                <div class="thumbnail">
                    <span class="favorites_mark goods-btn" data-seq="{{:content_seq }}" data-is-goods="{{:is_goods }}"><img src="/resources/img/icon-favorites{{if is_goods == 0}}-btn{{/if}}.svg" alt="즐겨찾기"></span>
                </div>
                <div class="info">
                    <p class="video_title">{{:title}}</p>
                    <div class="attaching">
                        <div>
                            <div>
                                <img src="/resources/img/icon-play.svg" alt="재생횟수">
                                <span>{{:view_cnt}}</span>
                            </div>
                            <div>
                                <img src="/resources/img/icon-favorites-grey.svg" alt="즐겨찾기수">
                                <span>{{:favorites_cnt}}</span>
                            </div>
                        </div>
                        <p class="price" style="display:none">{{dec:price}}원</p>
                    </div>
						{{if hashtags}}
                    		<div class="tag">
								{{for ~hashtag(hashtags)}}
                      	   			<div>{{eval:'#'}}{{:name}}</div>
								{{/for}}
                   	 		</div>
						{{/if}}
                </div>
            </div>
		{{/for}}
</script>