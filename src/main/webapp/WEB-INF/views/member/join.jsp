<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>

<link rel="stylesheet" href="/resources/css/joinus.css?v=${nowDate}">
<script src="/resources/js/common.js?v=${nowDate}"></script>
<script>
	var isPhoneAuth = false;
	var authRandNumber = "${nowDate}";
	
	$(function(){
		
		$(document).on("click", ".send-phone-auth-number", function(){
			
			var phone = $("input[name=phone]");
			if(!phone.val()){
				alert('전화번호 를 입력해주세요');
				phone.focus();
				return;
			}
			
			var param = {
					"phone":phone.val()
			}
			
			ajaxCallPost("/api/v1/member/auth/rand/number", param, function(res){
				if(res.success){
					alert("입력하신 휴대폰 번호로 인증번호가 발송되었습니다.");
					authRandNumber = res.data.authNumber;
					
					$(".send-phone-auth-number").text("다시받기");
				}
			})
			
		})
		$(document).on("click", ".auth-number-chk", function(){
			var authNumber = $("input[name=authNumber]");
			if(authNumber.val() == authRandNumber){
				alert('인증이 완료 되었습니다.')
				isPhoneAuth = true;
			}else{
				alert('인증이 실패 되었습니다.')
				isPhoneAuth = false;
			}
		})
		
		$(document).on("keyup", "input[name=phone]", function(){
			isPhoneAuth = false;
		})
		$(document).on("click", ".submit_btn", function(){
			
			if(!$("#terms1").is(":checked")){
				alert("라이프 에이드 개인정보수집 및 이용동의에\n동의하여 주세요.")
				return;
			}
			if(!$("#terms2").is(":checked")){
				alert("라이프 에이드 이용약관에\n동의하여 주세요.")
				return;
			}
			if(!$("#terms3").is(":checked")){
				alert("만 14세 이상 회원가입에\n동의하여 주세요.")
				return;
			}
			if(!isPhoneAuth){
				alert('전화번호 인증을 다시 시도해주세요.');
				return;
			}
			
			var phone = $("input[name=phone]");
			var param = {
					  "column_name":"phone",
					  "column_value":phone.val()
				}
			ajaxCallPost("/api/v1/common/chk", param, function(res){
				if(res.success){
					alert("전화번호가 기가입된 이력이 있습니다. 다른 전화번호로 입력바랍니다.");
				}else{
					$("input[name=hiddenPhone]").val(phone.val());
					document.form.submit();
				}
			})
			
			
			
			
		})
	})
	
</script>
<form name="form" action="/join2" method="POST">
	<input type="hidden" name="hiddenPhone" value="">
</form>
<div class="wrap">
    <div class="left"><img src="/resources/img/img1.daumcdn.jpg" alt=""></div>
    <div class="right">
        <button class="close_btn"><img src="/resources/img/close.svg" alt=""></button>
        <div class="position-center right-inner">
            <h2>회원가입</h2>
            <p class="joinus_txt">이미 가입을 하셨나요?<button onclick="location.href='/login'">로그인하기</button></p>
            <div class="terms_wrap">
                <div class="terms_all">
                    <input type="checkbox" id="terms_all">
                    <label for="terms_all"><span class="check_icon"></span><span>서비스 이용 전체동의</span></label>
                 </div>
                <div class="terms">
                   <div class="checkbox">
                        <input type="checkbox" id="terms1" name="terms_check">
                        <label for="terms1"><span class="check_icon"></span><span>라이프 에이드 개인정보수집 및 이용동의</span></label>
                   </div>
                   <button>보기</button>
                </div>
                <div class="terms">
                    <div class="checkbox">
                         <input type="checkbox" id="terms2" name="terms_check">
                         <label for="terms2"><span class="check_icon"></span><span>라이프 에이드 이용약관</span></label>
                    </div>
                    <button>보기</button>
                 </div>
                 <div class="terms">
                    <div class="checkbox">
                         <input type="checkbox" id="terms3" name="terms_check">
                         <label for="terms3"><span class="check_icon"></span><span>만 14세 이상 회원가입</span></label>
                    </div>
                    <button>보기</button>
                 </div>
            </div>
            <div class="certification">
                <div>
                    <input type="number" name="phone" class="txt_field" placeholder="휴대전화를 입력해주세요">
                    <button class="send-phone-auth-number">인증하기</button>
                </div>
                <div>
                    <input type="text" name="authNumber" class="txt_field" placeholder="인증번호를 입력해주세요">
                    <button class="auth-number-chk">인증완료</button>
                </div>
            </div>

            <input type="button" value="다음" class="submit_btn">
        </div>
    </div>
</div>
