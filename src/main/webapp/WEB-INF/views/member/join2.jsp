<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>

<link rel="stylesheet" href="/resources/css/joinus.css?v=${nowDate}">
<script src="/resources/js/common.js?v=${nowDate}"></script>

<script>
	$(function(){
		$(document).on("click", ".submit_btn", function(){
			var id = $("input[name=id]");
			var pw = $("input[name=pw]");
			var repw = $("input[name=repw]");
			
			if(!id.val()){
				alert('이메일을 입력해주세요');
				id.focus();
				return;
			}
			
			if(id.val().indexOf("@") == -1){
				alert('올바른 이메일 양식이 아닙니다');
				id.focus();
				return;
			}
			
			if(!pw.val()){
				alert('비밀번호를 입력해주세요');
				pw.focus();
				return;
			}
			
			if(!repw.val()){
				alert('비밀번호를 한번 더 입력해주세요');
				repw.focus();
				return;
			}
			
			if(pw.val() != repw.val()){
				alert('패스워드와 재입력 패스워드가 일치하지 않습니다.');
				repw.focus();
				return;
			}
			
			
			var param = {
				  "column_name":"id",
				  "column_value":id.val()
			}
			ajaxCallPost("/api/v1/common/chk", param, function(res){
				if(res.success){
					alert("Email 이 중복됩니다. 다른 Email로 입력바랍니다.");
				}else{
					var param = {
							"id":id.val(),
							"pw":pw.val(),
							"phone":"${param.hiddenPhone}"
					}
					ajaxCallPost("/api/v1/sign", param, function(res){
						if(res.success){
							location.href="/join3"
						}
					})
				}
			})
			
			
		})
	})
</script>

<div class="wrap">
    <div class="left"><img src="/resources/img/img1.daumcdn.jpg" alt=""></div>
    <div class="right">
        <button class="close_btn"><img src="img/close.svg" alt=""></button>
        <div class="position-center right-inner">
            <h2>회원가입</h2>
            <p class="joinus_txt">이미 가입을 하셨나요?<button onclick="javascript:location.href='/login'">로그인하기</button></p>
            <div>
                <input type="email" name="id" class="txt_field" placeholder="이메일을 입력해주세요">
            </div>
            <div>
                <input type="password" name="pw" class="txt_field" placeholder="비밀번호를 입력해주세요">
            </div>
            <div>
                <input type="password" name="repw" class="txt_field" placeholder="비밀번호를 다시 입력해주세요">
            </div>

            <input type="button" value="다음" class="submit_btn">
        </div>
    </div>
</div>
