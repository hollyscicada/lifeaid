<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>

<link rel="stylesheet" href="/resources/css/joinus.css?v=${nowDate}">
<script src="/resources/js/common.js?v=${nowDate}"></script>
<div class="wrap">
    <div class="left"><img src="/resources/img/img1.daumcdn.jpg" alt=""></div>
    <div class="right">
        <button class="close_btn"><img src="img/close.svg" alt=""></button>
        <div class="position-center right-inner completed">
            <div>
                <img src="/resources/img/logo.svg" alt="라이프에이드" class="logo">

                <p>라이프 에이드 회원가입을 축하드립니다.</p>
                <span>회원분들에게 좋은 컨텐츠로 보답하겠습니다.</span>
            </div>

            <input type="button" value="시작하기" class="submit_btn" onclick="javascript:location.href='/'">
        </div>

     
    </div>
</div>
