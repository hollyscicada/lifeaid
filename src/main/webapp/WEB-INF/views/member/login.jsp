<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>

<link rel="stylesheet" href="/resources/css/login.css?v=${nowDate}">

<script>
	var redirect_url = "${param.redirect_url}";
	$(function(){
		$(document).on("click", ".submit_btn", function(){
			var id = $("input[name=id]");
			var pw = $("input[name=pw]");
			
			if(!id.val()){
				alert('이메일을 입력해주세요');
				id.focus();
				return;
			}
			
			if(!pw.val()){
				alert('비밀번호를 입력해주세요');
				pw.focus();
				return;
			}
			
			var param = {
				"id":id.val(),
				"pw":pw.val()
			}
			ajaxCallPost("/api/v1/login", param, function(res){
				if(res.success){
					
					var remember_login = $("#remember_login").is(":checked");
					if(remember_login){
						localStorage.setItem("remember_login", "Y");
						localStorage.setItem("custom_pw", res.custom_pw);
					}else{
						localStorage.setItem("remember_login", null);
						localStorage.setItem("custom_pw", null);
						
					}
					if(redirect_url){
						location.href=decodeURI(redirect_url);
					}else{
						location.href="/";
					}
				}else{
					alert("이메일이나 비밀번호를 잘못입력하였습니다.")
				}
			})
			
		})
	})	
</script>

<script>
	function google(){
		location.href=`
			https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/userinfo.email&
			access_type=offline&
			include_granted_scopes=true&state=state_parameter_passthrough_value&
			redirect_uri=http://lifeaid.co.kr/google/oauth&response_type=code&client_id=1090409467460-sdqpc1fvpu4hoq5dp4vn4637vdk54rva.apps.googleusercontent.com
			`;
	}
	function facebook(){
		location.href="https://www.facebook.com/v9.0/dialog/oauth?client_id=3014556708830894&redirect_uri=http://localhost/facebook/oauth&state=a&resource_type=token";
	}
	function naver(){
		location.href="https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=ZJ5EefA7Lk3qOf_2bwFc&redirect_uri=http://lifeaid.co.kr/naver/oauth&state=a";
	}
	function kakao(){
		location.href="https://kauth.kakao.com/oauth/authorize?client_id=3aed439afb956eed42f7b72205d7083c&redirect_uri=http://lifeaid.co.kr/kakao/oauth&response_type=code";
	}
</script>

<div class="wrap">
    <div class="left"><img src="/resources/img/img1.daumcdn.jpg" alt=""></div>
    <div class="right">
        <button class="close_btn"><img src="/resources/img/close.svg" alt=""></button>
        <div class="right-inner position-center">
            <h2>로그인</h2>
            <p class="joinus_txt">아직 회원이 아니신가요?<button onclick="location.href='/join'">회원가입하기</button></p>

            <input type="text" name="id" placeholder="아이디" class="txt_field">
            <input type="password" name="pw"  placeholder="비밀번호" class="txt_field">
            <div class="remember_wrap">
                <div class="remember_login">
                    <input type="checkbox" id="remember_login" name="remember_login">
                    <label for="remember_login"><span class="check_icon"></span><span>자동 로그인</span></label>
                </div>
                <button onclick="location.href='/findId'">아이디/비밀번호 찾기</button>
            </div>
            <input type="button" value="로그인" class="submit_btn">

            <div class="sns_login">
                <p>SNS로 시작하기</p>
                <div class="btn_wrap">
                    <button onclick="javascript:google()">구글 로그인</button>
					<button onclick="javascript:facebook()">페이스북 로그인</button>
					<button onclick="javascript:naver()">네이버 로그인</button>
					<button onclick="javascript:kakao()">카카오톡 로그인</button>
                </div>
            </div>
        </div>
    </div>
</div>
