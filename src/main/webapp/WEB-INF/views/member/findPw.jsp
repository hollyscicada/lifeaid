<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>

 <link rel="stylesheet" href="/resources/css/FindUserInfo.css?v=${nowDate}">
 
 
 <script>
	$(function(){
		$(document).on("click", ".submit_btn", function(){
			var id = $("input[name=id]");
			var phone = $("input[name=phone]");
			if(!id.val()){
				alert('이메일을 입력해주세요');
				id.focus();
				return;
			}
			if(!phone.val()){
				alert('전화번호를 입력해주세요');
				phone.focus();
				return;
			}
			var param = {
					"id":id.val(),
					"phone":phone.val()
			}
			ajaxCallPost("/api/v1/find/pw", param, function(res){
				if(res.success){
					alert("해당 휴대폰으로 임시 비밀번호를 전송했습니다.");
				}else{
					alert("입력하신 정보로 일치하는 회원데이터가 없습니다.")
				}
			})
		})
	})
</script>
<div class="wrap">
    <div class="left"><img src="/resources/img/img1.daumcdn.jpg" alt=""></div>
    <div class="right">
        <button class="close_btn"><img src="/resources/img/close.svg" alt=""></button>
        <div class="position-center right-inner">
            <h2>비밀번호 찾기</h2>
            <p class="findpw_txt">아이디를 잊어버리셨나요?<button onclick="location.href='/findId'">아이디 찾기</button></p>

            <input type="text" name="id" placeholder="이메일을 입력해주세요" class="txt_field">
            <input type="tel"  name="phone" placeholder="휴대폰번호를 입력해주세요." class="txt_field">
            <span>계정에 등록되어있는 번호로 임시비밀번호를 전송드립니다.</span>
            <input type="button" value="비밀번호 찾기" class="submit_btn">

        </div>
    </div>
</div>
