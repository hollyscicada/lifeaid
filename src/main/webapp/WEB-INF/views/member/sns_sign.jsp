<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script>
	function google(){
		location.href=`
			https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/userinfo.email&
			access_type=offline&
			include_granted_scopes=true&state=state_parameter_passthrough_value&
			redirect_uri=http://lifeaid.co.kr/google/oauth&response_type=code&client_id=507526569391-u2bk2mdjf8pivdomnti3uru2vib75lu2.apps.googleusercontent.com
			`;
	}
	function facebook(){
		location.href="https://www.facebook.com/v9.0/dialog/oauth?client_id=918223885379245&redirect_uri=http://localhost/facebook/oauth&state=a&resource_type=token";
	}
	function naver(){
		location.href="https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=okK7wrJ0uQmmKxBcC8TI&redirect_uri=http://lifeaid.co.kr/naver/oauth&state=a";
	}
	function kakao(){
		location.href="https://kauth.kakao.com/oauth/authorize?client_id=d72266a6eadee95be4f0bbf5c8480d64&redirect_uri=http://lifeaid.co.kr/kakao/oauth&response_type=code";
	}
</script>

<div>
	<button onclick="javascript:google()">구글 로그인</button>
	<button onclick="javascript:facebook()">페이스북 로그인</button>
	<button onclick="javascript:naver()">네이버 로그인</button>
	<button onclick="javascript:kakao()">카카오톡 로그인</button>
</div>