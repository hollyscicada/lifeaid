<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
<link rel="stylesheet" href="/resources/css/main.css?v=${nowDate}">

<div>
    <!-- 배너 -->
    <div class="main_bnn">
        <div class="swiper-container swiper-banner">
            <div class="swiper-wrapper">
            	<c:forEach var="item" items="${bannerList }" >
		            <div class="swiper-slide" style="background-image : url('${item.img_url}')">${item.main_text }</div>
            	</c:forEach>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>

  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-banner', {
        slidesPerView: 1,
        loop: true,
        autoplay: {
            delay: 1500,
        },
        pagination: {
            el: '.swiper-pagination',
        },
    });
  </script>

	<div class="main_title">
	    <h2>새로워진 Life Aid,<br>새로워진 Health Care.</h2>
	    <p>새로운 기술과 트레이너, 그리고 방법론으로 가득 찬 라이프에이드를 소개합니다.</p>
	</div>
	<div class="swiper-container category">
	    <div class="swiper-wrapper category-wrapper">
	    
	    	<c:forEach var="item" items="${categoryList }" >
	    		<c:if test="${item.ebook_yn eq 'N' }">
		    		<div class="swiper-slide category_item"> 
			            <div class="img" style="background-image: url('${item.img_url }')"></div>
			            <p class="name">${item.name }</p>
			            <button onclick="javascript:location.href='/sub/${item.category_seq}'">자세히 보기</button>
			        </div>
		        </c:if>
           	</c:forEach>
           	
	    </div>
	</div>
	<script>
	    var swiper = new Swiper('.category', {
	        slidesPerView: 'auto',
	        touchRatio: 0,
	        breakpoints : {
	            768 : {
	                slidesPerView : 'auto',
	                touchRatio: 1,
	            },
	        },
	    }); 
	
	</script>
	<!-- 하단 정보 -->
	<div class="box_item img_left">
	    <div class="img"></div>
	    <div class="text">
	        <p class="sub_title">전문가가 알려주는 건강 정보</p>
	        <p>약 3000개의 풀HD 영상과 1000개 가량의 일러스트로<br>
	            구성된 전문적인 건강 정보</p>
	    </div>
	</div>
	<div class="box_item img_right">
	    <div class="img"></div>
	    <div class="text">
	        <p class="sub_title">뭘 좋아하실지 몰라서</p>
	        <p>맨몸 운동, 기구 운동, 체형교정, 재활, 마사지,<br> 
	            스트레칭 등 건강에 필수적인 모든 정보를 담았습니다.</p>
	    </div>
	</div>
	<div class="box_item img_left">
	    <div class="img"></div>
	    <div class="text">
	        <p class="sub_title">한땀 한땀 정리한 카테고리</p>
	        <p>간단하게 정리되어 있는 카테고리 분류로,<br> 
	            필요한 정보를 빠르게 찾아보세요</p>
	    </div>
	</div>
	<div class="box_item img_right">
	    <div class="img"></div>
	    <div class="text">
	        <p class="sub_title">직접 경험하고 해볼 수도 있는 운동</p>
	        <p>나만의 루틴을 저장하고 공유해 보세요<br>
	            체계적인 운동을 통해 원하는 목표를 달성할 수 있습니다.</p>
	    </div>
	</div>
</div>
