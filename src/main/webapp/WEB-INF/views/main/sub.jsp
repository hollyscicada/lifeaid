<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
<%@ include file="/WEB-INF/views/common/jsrender.jsp" %>

<link rel="stylesheet" href="/resources/css/sub.css?v=${nowDate}">
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	$(function(){
		$(document).on("click", ".hashtag-li", function(){
			$(".tbody").html('');
			$(".tbody2").html('');
			$(".hashtag-li").removeClass("active");
			$(this).addClass("active");
			
			var hashtag_seq = $(this).attr("data-seq");
			init("1", hashtag_seq);
		})	
		
		var ebook_yn = "${categoryRead.ebook_yn}";
		/*
			컨텐츠, 이북 분기
		*/
		if(ebook_yn == 'Y'){
			$(".price").show();
		}
		
	})
	function init(page, hashtag_seq){
		hashtag_seq = hashtag_seq == undefined ? null : hashtag_seq;
		
		var not_seqs = [];
		var seqObj = {
				"seq":"${todayContent.content_seq}"
		};
		not_seqs.push(seqObj)
		var param = {
				"page":"1",
				"page_block":"3",
				"category_seq":"${category_seq}",
				"sort":"dt",
				"hashtag_seq": hashtag_seq,
				"classify_type":"C|V",
				"access_code":"0|1",
				"not_seqs":not_seqs
			}
			
			ajaxCallPostToken("/api/v1/content", authToken, param, function(res){
				var template = $.templates("#accessCode"); // <!-- 템플릿 선언
		        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
		        $(".tbody").html(htmlOutput);
		        initSub(page, hashtag_seq)
			})
	}
	function initSub(page, hashtag_seq){
		var items = $(".tbody").find(".video_item");
		
		var not_seqs = [];
		var seqObj = {
				"seq":"${todayContent.content_seq}"
		};
		not_seqs.push(seqObj)
		
		for(var i = 0 ; i < items.length ; i++){
			var seqObj = {
					"seq":items.eq(i).attr("data-seq")
			};
			not_seqs.push(seqObj);
		}
		
		var param = {
				"page":page,
				"category_seq":"${category_seq}",
				"sort":"dt",
				"hashtag_seq": hashtag_seq,
				"classify_type":"C|V",
				"access_code":"2",
				"not_seqs":not_seqs
			}
			
			ajaxCallPostToken("/api/v1/content", authToken, param, function(res){
				var template = $.templates("#accessCode"); // <!-- 템플릿 선언
		        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
		        $(".tbody2").html(htmlOutput);
			})
	}
	
	init("1", null)
</script>
<div class="container">
   <div class="content">
	    <!-- 상단 검색 -->
	    <div class="search_wrap">
	        <h3 class="sub_title">${categoryRead.name }</h3>
	       <div class="search">
	            <input type="text">
	            <button><img src="/resources/img/icon-search2.svg" alt="검색" class="w-100"></button>
	       </div>
	    </div>
	    
	    
	    <!-- 오늘의 스팟 -->
	    <div class="main_video">
	        <div class="thumbnail" style="background-image: url('${todayContent.thumb_url}');background-size: 100% 100%;"></div>
	            <div class="info">
	                <div>
	                    <p class="video_title">${todayContent.title}</p>
	                    <p class="video_txt">${todayContent.sub_title}</p>
	                    <div class="attaching">
	                        <p>${todayContent.gubun_name }</p>
	                        <div>
	                            <img src="/resources/img/icon-play.svg" alt="재생횟수">
	                            <span>${todayContent.view_cnt }</span>
	                        </div>
	                        <div>
	                            <img src="/resources/img/icon-favorites-grey.svg" alt="즐겨찾기수">
	                            <span>${todayContent.routine_cnt }</span>
	                        </div>
	                    </div>
	                </div>
	                <div class="btn_wrap">
	                    <button class="more" onclick="javascript:location.href='/sub/detail/${todayContent.content_seq}'">자세히보기</button>
	                    <button data-seq="${todayContent.content_seq }" data-is-goods="${todayContent.is_goods }" class="goods-btn">
	                    	<img src="/resources/img/icon-favorites<c:if test="${todayContent.is_goods == 0 }">-btn</c:if>.svg" alt="즐겨찾기">
	                    </button>
	                    <button><img src="/resources/img/icon-routine-share.svg" alt="루틴공유"></button>
	                </div>
	            </div>
	    </div>
	    
	    
	    <!-- 광고 -->
	    <div class="ad window-open" style="background-image: url('${adzoneRead.img_url}');background-size: 100% 100%;" data-link="${adzoneRead.link_url }"></div>
	
	   <div class="list_wrap">
	   
	   		<!-- 해시태그  리스트 -->
	       <div class="filter">
	           <button class="active hashtag-li">#전체</button>
	           <c:forEach var="item" items="${hashTagList }">
		           <button class="hashtag-li" data-seq="${item.hashtag_seq }">#${item.name }</button>
	           </c:forEach>
	       </div>
	       
	       <!-- 해시태그 무료 컨텐츠 리스트 -->
	       <div class="video_list tbody"></div>
	       
	       <!-- 해시태그 유/무료 컨텐츠 리스트 -->
	       <div class="video_list tbody2"></div>
	   </div>
	   
	   
	   
	</div>

	<!-- 오른쪽 영상리스트 -->
	<%@ include file="/WEB-INF/views/common/right.jsp" %>
</div>
  
