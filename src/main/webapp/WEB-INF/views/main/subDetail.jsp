<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
<%@ include file="/WEB-INF/views/common/jsrender.jsp" %>

<link rel="stylesheet" href="/resources/css/subDetail.css?v=${nowDate}">
<style>
	.video-txt img{
		width: 50%
	}
</style>
<script>
	$(function(){
		var val = `${contentRead.content}`;
		var content = val.replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&#039;/g,"'").replace(/&quot;/g,"\"");
		$(".video-txt").html(content);
		$(".video-text-mobile").html(content);
	})
</script>
<script>
	var authToken = "${sessionScope.MEMBER.authToken}";
	$(function(){
		var classify_type = "${contentRead.classify_type}";
		/*
			일반, 영상, 이북 분기
		*/
		if(classify_type == 'C'){
			$(".price").show();
		}else if(classify_type == 'V'){
			$(".video-div").show();
			$(".price").show();
		}else if(classify_type == 'E'){
			
		}
		
	})
	function init(page){
		
		var param = {
				"page":page,
				"page_block":"4",
				"content_seq":"${content_seq}"
			}
			
			ajaxCallPostToken("/api/v1/content/relation", authToken, param, function(res){
				var template = $.templates("#accessCode"); // <!-- 템플릿 선언
		        var htmlOutput = template.render(res); // <!-- 렌더링 진행 -->
		        $(".tbody").html(htmlOutput);
			})
	}
	init("1", null)
</script>
<div class="container">
   <div class="content">
	    <p class="route">
	        <span>${contentRead.category_name }</span> > <span>${contentRead.info_name }</span> > <span>${contentRead.gubun_name }</span>
	    </p>
	
	    <!-- 영상 -->
	    <div class="video" >
	        <div class="title">
	            <p>${contentRead.title }</p>
	            <div class="btn_wrap">
	                <button class="goods-btn" data-seq="${contentRead.content_seq }" data-is-goods="${contentRead.is_goods }">
	                	<img src="/resources/img/icon-favorites<c:if test="${contentRead.is_goods == 0 }">-btn</c:if>.svg" alt="즐겨찾기">
	                </button>
	                <button><img src="/resources/img/icon-share.svg" alt="공유"></button>
	                <button><img src="/resources/img/icon-routine-share.svg" alt="루틴공유"></button>
	            </div>
	        </div>
	        <div class="player video-div"  style="display: none">
				<video style="width:100%;height: 100%;" poster="${contentRead.thumb_file_url}" controls>
				  <source src="${contentRead.file_url}" type="video/mp4">
				</video>
	        </div>
	        <div class="info">
	            <div class="attaching">
	                <p>${contentRead.gubun_name }</p>
	                <div>
	                    <img src="/resources/img/icon-play.svg" alt="재생횟수">
	                    <span>${contentRead.view_cnt}</span>
	                </div>
	                <div>
	                    <img src="/resources/img/icon-favorites-grey.svg" alt="즐겨찾기수">
	                    <span>${contentRead.favorites_cnt }</span>
	                </div>
	            </div>
	            <div class="video-txt">
	            </div>
	        </div>
	    </div>
	
	    <!-- 모바일 영상 -->
	    <div class="video mobile_video">
	        <div class="thumbnail video-div"  style="display: none">
	        	<video style="width:100%;height: 100%;" poster="${contentRead.thumb_file_url}" controls>
				  <source src="${contentRead.file_url}" type="video/mp4">
				</video>
	        </div>
	        <p class="route">
	            <span>${contentRead.category_name }</span> > <span>${contentRead.info_name }</span> > <span>${contentRead.gubun_name }</span>
	        </p>
	            <div class="info">
	                <div>
	                    <p class="title">${contentRead.title }</p>
	                    <p class="text video-text-mobile"></p>
	                    <div class="attaching">
	                        <p>${contentRead.gubun_name }</p>
	                        <div>
	                            <img src="/resources/img/icon-play.svg" alt="재생횟수">
	                            <span>${contentRead.view_cnt}</span>
	                        </div>
	                        <div>
	                            <img src="/resources/img/icon-favorites-grey.svg" alt="즐겨찾기수">
	                            <span>${contentRead.favorites_cnt }</span>
	                        </div>
	                    </div>
	                </div>
	                <div class="btn_wrap">
	                    <button class="goods-btn" data-seq="${contentRead.content_seq }" data-is-goods="${contentRead.is_goods }">
	                    	<img src="/resources/img/icon-favorites<c:if test="${contentRead.is_goods == 0 }">-btn</c:if>.svg" alt="즐겨찾기">
	                    </button>
	                    <button><img src="/resources/img/icon-share.svg" alt="공유"></button>
	                    <button><img src="/resources/img/icon-routine-share.svg" alt="루틴공유"></button>
	                </div>
	            </div>
	    </div>
	
	    
	    
	    <!-- LiveRe City install code -->
		<div id="lv-container" data-id="city" data-uid="MTAyMC81Mjc3NS8yOTI1Mg==" class="comment" style="border:unset">
			<script type="text/javascript">
			   (function(d, s) {
			       var j, e = d.getElementsByTagName(s)[0];
			
			       if (typeof LivereTower === 'function') { return; }
			
			       j = d.createElement(s);
			       j.src = 'https://cdn-city.livere.com/js/embed.dist.js';
			       j.async = true;
			
			       e.parentNode.insertBefore(j, e);
			   })(document, 'script');
			</script>
			<noscript>Please activate JavaScript for write a comment in LiveRe</noscript>
		</div>
		<!-- completed City install code -->
	    
	    
	    
	
	        <!-- 영상리스트 -->
	   <div class="list_wrap">
	    <div class="video_list tbody">
	    </div>
	</div>
   </div>

   
	<!-- 오른쪽 영상리스트 -->
	<%@ include file="/WEB-INF/views/common/right.jsp" %>
</div>
