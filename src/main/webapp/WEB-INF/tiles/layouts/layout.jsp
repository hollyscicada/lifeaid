<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="today" class="java.util.Date" />
<fmt:formatDate value="${today}" pattern="yyyyMMddHHmmss" var="nowDate"/>

<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>라이프에이드</title>
    <link rel="stylesheet" href="/resources/css/swiper.min.css?v=${nowDate}" />
    <link rel="stylesheet" href="/resources/css/reset.css?v=${nowDate}">
    <link rel="stylesheet" href="/resources/css/font.css?v=${nowDate}">
    
    <script src="/resources/js/jquery-3.5.1.min.js"></script>
    <script src="/resources/js/swiper.min.js"></script>
    <script src="/resources/js/common.js?v=${nowDate}"></script>

	<script src="/resources/js/commonFront.js?v=${nowDate}"></script>
	<script src="/resources/js/commonAjax.js?v=${nowDate}"></script>
	<script src="https://www.jsviews.com/download/jsrender.js"></script>

</head>

<body>
    <!-- Main wrapper  -->
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="content" />
		<tiles:insertAttribute name="footer" />

</body>

</html>
