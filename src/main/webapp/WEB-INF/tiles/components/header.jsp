<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
<script>
	$(function(){
		var remember_login = localStorage.getItem("remember_login");
		var sessionId = "${sessionScope.MEMBER.id}";
		var custom_pw = localStorage.getItem("custom_pw");
		if(!sessionId && remember_login == 'Y'){
			var param = {
					"custom_pw":custom_pw
			}
			ajaxCallPost("/api/v1/login/auto", param, function(res){
				if(res.success){
					location.reload();
				}
			})
			
			
		}
	})
</script>
<header>
  <div class="header">
    <!-- 웹 메뉴 -->
    <img src="/resources/img/header_logo.svg" alt="라이프에이드" class="header_logo" onclick="location.href='/'">
    <ul class="nav">
    	<c:forEach var="item" items="${categoryList }" >
	    	<li><a href="/sub/${item.category_seq}">${item.name }</a></li>
        </c:forEach>
        <li><a class="sports-item" style="cursor: pointer; ">운동용품</a></li>
    </ul>
    <!-- 모바일 메뉴 -->
    <div class="mobile_menu">
        <button><img src="/resources/img/m_menu_btn.svg" alt="메뉴" class="munu_btn"></button>
        <img src="/resources/img/logo.svg" alt="라이프에이드" class="header_logo" onclick="location.href='/'">
    </div>
    <ul class="mobile_nav">
    	<c:forEach var="item" items="${categoryList }" >
    		<c:choose>
    			<c:when test="${fn:length(item.infoList) > 0 }">
			    	<li>
			            <button>
			                <div>
			                    <img src="/resources/img/m_menu1.svg" alt="${item.name }">${item.name }
			                    <img src="/resources/img/arrow_down.svg" alt="메뉴" class="menu_arrow">
			                </div>
			                <ul class="drop_menu">
			                    <li><a href="#">전체</a></li>
			                    <c:forEach var="info" items="${item.infoList }" >
			                    	<li><a href="#">${info.name }</a></li>
			                    </c:forEach>
			                </ul>
			            </button>
			        </li>
		        </c:when>
		        <c:otherwise>
		        	<li><a href="/sub/${item.category_seq}"><img src="/resources/img/m_menu5.svg" alt="${item.name }">${item.name }</a></li>
		        </c:otherwise>
	        </c:choose>
        </c:forEach>
        <li><a class="sports-item" style="cursor: pointer; "><img src="/resources/img/m_menu6.svg" alt="운동용품">운동용품</a></li>
    </ul>

    <div class="profile">
        <button class="serch_btn"><img src="/resources/img/icon-search.svg" alt="검색"></button>
        <div class="user_img">
            <img src="/resources/img/img1.daumcdn.jpg" alt="" class="position-center">
        </div>
    </div>

    <ul class="my_menu">
        <li>
            <a href="#">
                <img src="/resources/img/my_menu1_off.svg" alt="내프로필"><span>내프로필</span>
            </a>
        </li>
        <li>
            <a href="#">
                <img src="/resources/img/my_menu2_off.svg" alt="프리미엄 구독하기"><span>프리미엄 구독하기</span>
            </a>
        </li>
        <li>
            <a href="#">
                <img src="/resources/img/my_menu3_off.svg" alt="설정"><span>설정</span>
            </a>
        </li>
        <li>
            <a href="#">
                <img src="/resources/img/my_menu4_off.svg" alt="CS문의"><span>CS문의</span>
            </a>
        </li>
        <li>
            <a href="#">
                <img src="/resources/img/my_menu5_off.svg" alt="구매내역"><span>구매내역</span>
            </a>
        </li>
        <li>
            <a href="/logout">
                <img src="/resources/img/my_menu6_off.svg" alt="로그아웃"><span>로그아웃</span>
            </a>
        </li>
    </ul>
  </div>
</header>