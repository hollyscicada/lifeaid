<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/jstl.jsp" %>
<footer>
    <div class="footer">
        <div class="store_btn">
            <button><img src="/resources/img/app_store1.svg" alt="플레이스토어에서 받기"><span>플레이스토어에서 받기</span></button>
            <button><img src="/resources/img/app_store2.svg" alt="앱스토어에서 받기"><span>앱스토어에서 받기</span></button>
        </div>
        <img src="/resources/img/header_logo.svg" alt="라이프에이드" class="foo_logo">
        <p>체형교정 재활 운동 스트레칭까지 건강 위키백과</p>
        <ul class="foo_nav">
            <li><a href="#">회사소개</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">이용약관</a></li>
            <li><a href="#">개인정보처리방침</a></li>
        </ul>
        <span class="line"></span>
        <div class="foo_info">
            <span>대표 : 전하윤</span>
            <span>상호 : 라이프 에이드 </span>   
            <span>사업자등록번호 : XXX-XX-XXXXX</span>    
            <span>통신판매업 : 2020-서울OO-XXXX</span>
            <span>전자우편 : $이메일 주소</span>    
            <span>주소 : 서울특별시 OO구  OO동  OOOOOOOOOO </span>
            <span>콜센터 : XXXX-XXXX    평일 : AM 09:00~ PM 17:00    점심시간 : PM12:00~PM13:00    토/일/공휴일 휴무</span>
        </div>
    </div>
</footer>