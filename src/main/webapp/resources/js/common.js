$(function(){
    // 전체동의
    $("#terms_all").on('change',function(){

        if($(this).is(':checked')){
            $('input[name = terms_check]').prop('checked', true);
        }else{
            $('input[name = terms_check]').prop('checked', false);
        }
    });
    $('input[name = terms_check]').change(function () {

        var checkedLength = $('input[name="terms_check"]:checked').length;
        if(checkedLength == 3){
            $("#terms_all").prop('checked', true);
        }else{
            $("#terms_all").prop('checked', false);
        }
    });

    // 마이메뉴
    var isClick = false;
    $('.my_menu > li').mouseover(function () {
        $(this).children().css({'color':'#00a2e9'})
        $(this).find('img').attr("src", $(this).find('img').attr("src").replace("_off.svg","_on.svg"));
    });
    $('.my_menu > li').mouseout(function () {
        if(!isClick) {
            $(this).children().css({'color':'#000000'})
            $(this).find('img').attr("src", $(this).find('img').attr("src").replace("_on.svg","_off.svg"));
          }
        
    });

    $('.my_menu > li').click(function () {
        $(this).css({'background':'rgba(0,162,233,0.2)'})
        $(this).children().css({'color':'#00a2e9'})
        $(this).find('img').attr("src", $(this).find('img').attr("src").replace("_off.svg","_on.svg"));
        isClick = true;
    });


    $('.user_img').on('click', function(){
        $('.my_menu').toggle();
    });

    // 모바일 header menu
    $('.munu_btn').on('click', function(){
        $('.mobile_nav').slideToggle(100, function(){
            if($(this).is(':visible')){
                $('.munu_btn').attr("src", $('.munu_btn').attr("src").replace("_btn.svg","_close.svg"));
            } else {
                $('.munu_btn').attr("src", $('.munu_btn').attr("src").replace("_close.svg","_btn.svg"));
            }           
        });

        $('.mobile_nav li button').removeClass('active');
        $('.drop_menu').css({'display':'none'});

    });


    $('.mobile_nav li button').on('click', function(e){

        e.preventDefault();
        
        $(this).toggleClass('active');
        $(this).children('.drop_menu').slideToggle(100);

    });

    $(window).resize(function() {
        if($(window).width() > 769) {
            $('.mobile_nav').css({'display':'none'});
            $('.munu_btn').attr("src", $('.munu_btn').attr("src").replace("_close.svg","_btn.svg"));
        }
    });

})