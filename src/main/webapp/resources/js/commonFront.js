$(function(){
    // 운동용품 사이트 연결
    $(".sports-item").on('click',function(){
    	window.open('https://instashop.srookpay.com/naum02988_18')
    });
    $(".window-open").on('click',function(){
    	window.open($(this).attr("data-link"))
    });
    $(document).on('click', '.goods-btn', function(event){
    	event.stopPropagation();
    	var obj = $(this);
    	if(!authToken){
    		alert('즐겨찾기 추가/삭제 시에는 로그인이 필요합니다.')
    		return;
    	}
    	
    	var content_seq = obj.attr("data-seq");
    	var is_goods = obj.attr("data-is-goods");
    	var url = "";
    	if(is_goods>0){
    		url = "/api/v1/favorites/delete"
    	}else{
    		url = "/api/v1/favorites/create";
    	}
    	
    	
    	
    	var param = {
    			"content_seq":content_seq,
    			"folder_seq":"1"
    	}
    	
    	ajaxCallPostToken(url, authToken, param, function(res){
    		if(res.success){
    			if(is_goods<=0){
    				obj.find("img").attr("src", "/resources/img/icon-favorites.svg")
    				obj.attr("data-is-goods", "1")
    			}else{
    				obj.find("img").attr("src", "/resources/img/icon-favorites-btn.svg")
    				obj.attr("data-is-goods", "0")
    			}
    		}
    	})
    	
    });

})

var global = {
		comma:function(str) {
		    str = String(str);
		    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
		},
		hipn:function(str) {
			return str.replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-");
		}
}